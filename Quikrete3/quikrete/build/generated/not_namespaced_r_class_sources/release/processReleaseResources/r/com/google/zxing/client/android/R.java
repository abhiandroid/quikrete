/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.zxing.client.android;

public final class R {
    private R() {}

    public static final class array {
        private array() {}

        public static final int country_codes = 0x7f030008;
        public static final int preferences_front_light_options = 0x7f030009;
        public static final int preferences_front_light_values = 0x7f03000a;
    }
    public static final class attr {
        private attr() {}

        public static final int ambientEnabled = 0x7f040029;
        public static final int buttonSize = 0x7f04004b;
        public static final int cameraBearing = 0x7f040050;
        public static final int cameraTargetLat = 0x7f040051;
        public static final int cameraTargetLng = 0x7f040052;
        public static final int cameraTilt = 0x7f040053;
        public static final int cameraZoom = 0x7f040054;
        public static final int circleCrop = 0x7f040057;
        public static final int colorScheme = 0x7f040069;
        public static final int imageAspectRatio = 0x7f0400c1;
        public static final int imageAspectRatioAdjust = 0x7f0400c2;
        public static final int liteMode = 0x7f0400e7;
        public static final int mapType = 0x7f0400ec;
        public static final int scopeUris = 0x7f040118;
        public static final int uiCompass = 0x7f040181;
        public static final int uiMapToolbar = 0x7f040182;
        public static final int uiRotateGestures = 0x7f040183;
        public static final int uiScrollGestures = 0x7f040184;
        public static final int uiTiltGestures = 0x7f040185;
        public static final int uiZoomControls = 0x7f040186;
        public static final int uiZoomGestures = 0x7f040187;
        public static final int useViewLifecycle = 0x7f040189;
        public static final int zOrderOnTop = 0x7f040196;
    }
    public static final class color {
        private color() {}

        public static final int common_action_bar_splitter = 0x7f060036;
        public static final int common_google_signin_btn_text_dark = 0x7f060037;
        public static final int common_google_signin_btn_text_dark_default = 0x7f060038;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f060039;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f06003a;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f06003b;
        public static final int common_google_signin_btn_text_light = 0x7f06003c;
        public static final int common_google_signin_btn_text_light_default = 0x7f06003d;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f06003e;
        public static final int common_google_signin_btn_text_light_focused = 0x7f06003f;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f060040;
        public static final int common_plus_signin_btn_text_dark = 0x7f060041;
        public static final int common_plus_signin_btn_text_dark_default = 0x7f060042;
        public static final int common_plus_signin_btn_text_dark_disabled = 0x7f060043;
        public static final int common_plus_signin_btn_text_dark_focused = 0x7f060044;
        public static final int common_plus_signin_btn_text_dark_pressed = 0x7f060045;
        public static final int common_plus_signin_btn_text_light = 0x7f060046;
        public static final int common_plus_signin_btn_text_light_default = 0x7f060047;
        public static final int common_plus_signin_btn_text_light_disabled = 0x7f060048;
        public static final int common_plus_signin_btn_text_light_focused = 0x7f060049;
        public static final int common_plus_signin_btn_text_light_pressed = 0x7f06004a;
        public static final int contents_text = 0x7f06004b;
        public static final int encode_view = 0x7f06005b;
        public static final int place_autocomplete_prediction_primary_text = 0x7f06007a;
        public static final int place_autocomplete_prediction_primary_text_highlight = 0x7f06007b;
        public static final int place_autocomplete_prediction_secondary_text = 0x7f06007c;
        public static final int place_autocomplete_search_hint = 0x7f06007d;
        public static final int place_autocomplete_search_text = 0x7f06007e;
        public static final int place_autocomplete_separator = 0x7f06007f;
        public static final int possible_result_points = 0x7f060080;
        public static final int result_minor_text = 0x7f06008d;
        public static final int result_points = 0x7f06008e;
        public static final int result_text = 0x7f06008f;
        public static final int result_view = 0x7f060090;
        public static final int status_text = 0x7f06009a;
        public static final int transparent = 0x7f0600a3;
        public static final int viewfinder_laser = 0x7f0600a4;
        public static final int viewfinder_mask = 0x7f0600a5;
    }
    public static final class dimen {
        private dimen() {}

        public static final int half_padding = 0x7f070090;
        public static final int place_autocomplete_button_padding = 0x7f0700aa;
        public static final int place_autocomplete_powered_by_google_height = 0x7f0700ab;
        public static final int place_autocomplete_powered_by_google_start = 0x7f0700ac;
        public static final int place_autocomplete_prediction_height = 0x7f0700ad;
        public static final int place_autocomplete_prediction_horizontal_margin = 0x7f0700ae;
        public static final int place_autocomplete_prediction_primary_text = 0x7f0700af;
        public static final int place_autocomplete_prediction_secondary_text = 0x7f0700b0;
        public static final int place_autocomplete_progress_horizontal_margin = 0x7f0700b1;
        public static final int place_autocomplete_progress_size = 0x7f0700b2;
        public static final int place_autocomplete_separator_start = 0x7f0700b3;
        public static final int standard_padding = 0x7f0700b5;
    }
    public static final class drawable {
        private drawable() {}

        public static final int common_full_open_on_phone = 0x7f080079;
        public static final int common_google_signin_btn_icon_dark = 0x7f08007a;
        public static final int common_google_signin_btn_icon_dark_disabled = 0x7f08007b;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f08007c;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f08007d;
        public static final int common_google_signin_btn_icon_dark_pressed = 0x7f08007e;
        public static final int common_google_signin_btn_icon_light = 0x7f08007f;
        public static final int common_google_signin_btn_icon_light_disabled = 0x7f080080;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f080081;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f080082;
        public static final int common_google_signin_btn_icon_light_pressed = 0x7f080083;
        public static final int common_google_signin_btn_text_dark = 0x7f080084;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f080085;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f080086;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f080087;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f080088;
        public static final int common_google_signin_btn_text_light = 0x7f080089;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f08008a;
        public static final int common_google_signin_btn_text_light_focused = 0x7f08008b;
        public static final int common_google_signin_btn_text_light_normal = 0x7f08008c;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f08008d;
        public static final int common_ic_googleplayservices = 0x7f08008e;
        public static final int common_plus_signin_btn_icon_dark = 0x7f08008f;
        public static final int common_plus_signin_btn_icon_dark_disabled = 0x7f080090;
        public static final int common_plus_signin_btn_icon_dark_focused = 0x7f080091;
        public static final int common_plus_signin_btn_icon_dark_normal = 0x7f080092;
        public static final int common_plus_signin_btn_icon_dark_pressed = 0x7f080093;
        public static final int common_plus_signin_btn_icon_light = 0x7f080094;
        public static final int common_plus_signin_btn_icon_light_disabled = 0x7f080095;
        public static final int common_plus_signin_btn_icon_light_focused = 0x7f080096;
        public static final int common_plus_signin_btn_icon_light_normal = 0x7f080097;
        public static final int common_plus_signin_btn_icon_light_pressed = 0x7f080098;
        public static final int common_plus_signin_btn_text_dark = 0x7f080099;
        public static final int common_plus_signin_btn_text_dark_disabled = 0x7f08009a;
        public static final int common_plus_signin_btn_text_dark_focused = 0x7f08009b;
        public static final int common_plus_signin_btn_text_dark_normal = 0x7f08009c;
        public static final int common_plus_signin_btn_text_dark_pressed = 0x7f08009d;
        public static final int common_plus_signin_btn_text_light = 0x7f08009e;
        public static final int common_plus_signin_btn_text_light_disabled = 0x7f08009f;
        public static final int common_plus_signin_btn_text_light_focused = 0x7f0800a0;
        public static final int common_plus_signin_btn_text_light_normal = 0x7f0800a1;
        public static final int common_plus_signin_btn_text_light_pressed = 0x7f0800a2;
        public static final int launcher_icon = 0x7f0800bc;
        public static final int places_ic_clear = 0x7f0800da;
        public static final int places_ic_search = 0x7f0800db;
        public static final int powered_by_google_dark = 0x7f0800dc;
        public static final int powered_by_google_light = 0x7f0800dd;
        public static final int share_via_barcode = 0x7f0800ee;
    }
    public static final class id {
        private id() {}

        public static final int adjust_height = 0x7f090021;
        public static final int adjust_width = 0x7f090022;
        public static final int app_picker_list_item_icon = 0x7f090026;
        public static final int app_picker_list_item_label = 0x7f090027;
        public static final int auto = 0x7f090029;
        public static final int barcode_image_view = 0x7f09002a;
        public static final int bookmark_title = 0x7f09002d;
        public static final int bookmark_url = 0x7f09002e;
        public static final int contents_supplement_text_view = 0x7f090056;
        public static final int contents_text_view = 0x7f090057;
        public static final int dark = 0x7f09005b;
        public static final int decode = 0x7f09005c;
        public static final int decode_failed = 0x7f09005d;
        public static final int decode_succeeded = 0x7f09005e;
        public static final int format_text_view = 0x7f090079;
        public static final int help_contents = 0x7f09007d;
        public static final int history_detail = 0x7f09007e;
        public static final int history_title = 0x7f09007f;
        public static final int hybrid = 0x7f090082;
        public static final int icon_only = 0x7f090086;
        public static final int image_view = 0x7f090089;
        public static final int launch_product_query = 0x7f090097;
        public static final int light = 0x7f090099;
        public static final int menu_encode = 0x7f0900cd;
        public static final int menu_help = 0x7f0900ce;
        public static final int menu_history = 0x7f0900cf;
        public static final int menu_history_clear_text = 0x7f0900d0;
        public static final int menu_history_send = 0x7f0900d1;
        public static final int menu_settings = 0x7f0900d2;
        public static final int menu_share = 0x7f0900d3;
        public static final int meta_text_view = 0x7f0900d6;
        public static final int meta_text_view_label = 0x7f0900d7;
        public static final int none = 0x7f0900dd;
        public static final int normal = 0x7f0900de;
        public static final int page_number_view = 0x7f0900e4;
        public static final int place_autocomplete_clear_button = 0x7f0900e9;
        public static final int place_autocomplete_powered_by_google = 0x7f0900ea;
        public static final int place_autocomplete_prediction_primary_text = 0x7f0900eb;
        public static final int place_autocomplete_prediction_secondary_text = 0x7f0900ec;
        public static final int place_autocomplete_progress = 0x7f0900ed;
        public static final int place_autocomplete_search_button = 0x7f0900ee;
        public static final int place_autocomplete_search_input = 0x7f0900ef;
        public static final int place_autocomplete_separator = 0x7f0900f0;
        public static final int preview_view = 0x7f0900f3;
        public static final int query_button = 0x7f0900fa;
        public static final int query_text_view = 0x7f0900fb;
        public static final int quit = 0x7f0900fc;
        public static final int restart_preview = 0x7f0900ff;
        public static final int result_button_view = 0x7f090100;
        public static final int result_list_view = 0x7f090101;
        public static final int result_view = 0x7f090102;
        public static final int return_scan_result = 0x7f090103;
        public static final int satellite = 0x7f090107;
        public static final int share_app_button = 0x7f09011f;
        public static final int share_bookmark_button = 0x7f090120;
        public static final int share_clipboard_button = 0x7f090121;
        public static final int share_contact_button = 0x7f090122;
        public static final int share_text_view = 0x7f090123;
        public static final int snippet_view = 0x7f09012d;
        public static final int standard = 0x7f090133;
        public static final int status_view = 0x7f090136;
        public static final int terrain = 0x7f09013b;
        public static final int time_text_view = 0x7f090144;
        public static final int type_text_view = 0x7f090178;
        public static final int viewfinder_view = 0x7f090182;
        public static final int wide = 0x7f090188;
        public static final int wrap_content = 0x7f09018a;
    }
    public static final class integer {
        private integer() {}

        public static final int google_play_services_version = 0x7f0a0007;
    }
    public static final class layout {
        private layout() {}

        public static final int app_picker_list_item = 0x7f0b0032;
        public static final int bookmark_picker_list_item = 0x7f0b0033;
        public static final int capture = 0x7f0b0035;
        public static final int encode = 0x7f0b0047;
        public static final int help = 0x7f0b0049;
        public static final int history_list_item = 0x7f0b004a;
        public static final int place_autocomplete_fragment = 0x7f0b006b;
        public static final int place_autocomplete_item_powered_by_google = 0x7f0b006c;
        public static final int place_autocomplete_item_prediction = 0x7f0b006d;
        public static final int place_autocomplete_progress = 0x7f0b006e;
        public static final int search_book_contents = 0x7f0b0070;
        public static final int search_book_contents_header = 0x7f0b0071;
        public static final int search_book_contents_list_item = 0x7f0b0072;
        public static final int share = 0x7f0b0076;
    }
    public static final class menu {
        private menu() {}

        public static final int capture = 0x7f0c0004;
        public static final int encode = 0x7f0c0005;
        public static final int history = 0x7f0c0006;
    }
    public static final class raw {
        private raw() {}

        public static final int beep = 0x7f0d0000;
        public static final int gtm_analytics = 0x7f0d0001;
    }
    public static final class string {
        private string() {}

        public static final int app_name = 0x7f0e001e;
        public static final int app_picker_name = 0x7f0e001f;
        public static final int auth_google_play_services_client_facebook_display_name = 0x7f0e0021;
        public static final int auth_google_play_services_client_google_display_name = 0x7f0e0022;
        public static final int bookmark_picker_name = 0x7f0e0023;
        public static final int button_add_calendar = 0x7f0e0025;
        public static final int button_add_contact = 0x7f0e0026;
        public static final int button_book_search = 0x7f0e0027;
        public static final int button_cancel = 0x7f0e0028;
        public static final int button_custom_product_search = 0x7f0e0029;
        public static final int button_dial = 0x7f0e002a;
        public static final int button_email = 0x7f0e002b;
        public static final int button_get_directions = 0x7f0e002c;
        public static final int button_mms = 0x7f0e002d;
        public static final int button_ok = 0x7f0e002e;
        public static final int button_open_browser = 0x7f0e002f;
        public static final int button_product_search = 0x7f0e0030;
        public static final int button_search_book_contents = 0x7f0e0031;
        public static final int button_share_app = 0x7f0e0032;
        public static final int button_share_bookmark = 0x7f0e0033;
        public static final int button_share_by_email = 0x7f0e0034;
        public static final int button_share_by_sms = 0x7f0e0035;
        public static final int button_share_clipboard = 0x7f0e0036;
        public static final int button_share_contact = 0x7f0e0037;
        public static final int button_show_map = 0x7f0e0038;
        public static final int button_sms = 0x7f0e0039;
        public static final int button_web_search = 0x7f0e003a;
        public static final int button_wifi = 0x7f0e003b;
        public static final int common_google_play_services_api_unavailable_text = 0x7f0e004d;
        public static final int common_google_play_services_enable_button = 0x7f0e004e;
        public static final int common_google_play_services_enable_text = 0x7f0e004f;
        public static final int common_google_play_services_enable_title = 0x7f0e0050;
        public static final int common_google_play_services_install_button = 0x7f0e0051;
        public static final int common_google_play_services_install_text_phone = 0x7f0e0052;
        public static final int common_google_play_services_install_text_tablet = 0x7f0e0053;
        public static final int common_google_play_services_install_title = 0x7f0e0054;
        public static final int common_google_play_services_invalid_account_text = 0x7f0e0055;
        public static final int common_google_play_services_invalid_account_title = 0x7f0e0056;
        public static final int common_google_play_services_network_error_text = 0x7f0e0057;
        public static final int common_google_play_services_network_error_title = 0x7f0e0058;
        public static final int common_google_play_services_notification_ticker = 0x7f0e0059;
        public static final int common_google_play_services_restricted_profile_text = 0x7f0e005a;
        public static final int common_google_play_services_restricted_profile_title = 0x7f0e005b;
        public static final int common_google_play_services_sign_in_failed_text = 0x7f0e005c;
        public static final int common_google_play_services_sign_in_failed_title = 0x7f0e005d;
        public static final int common_google_play_services_unknown_issue = 0x7f0e005e;
        public static final int common_google_play_services_unsupported_text = 0x7f0e005f;
        public static final int common_google_play_services_unsupported_title = 0x7f0e0060;
        public static final int common_google_play_services_update_button = 0x7f0e0061;
        public static final int common_google_play_services_update_text = 0x7f0e0062;
        public static final int common_google_play_services_update_title = 0x7f0e0063;
        public static final int common_google_play_services_updating_text = 0x7f0e0064;
        public static final int common_google_play_services_updating_title = 0x7f0e0065;
        public static final int common_google_play_services_wear_update_text = 0x7f0e0066;
        public static final int common_open_on_phone = 0x7f0e0067;
        public static final int common_signin_button_text = 0x7f0e0068;
        public static final int common_signin_button_text_long = 0x7f0e0069;
        public static final int contents_contact = 0x7f0e006a;
        public static final int contents_email = 0x7f0e006b;
        public static final int contents_location = 0x7f0e006c;
        public static final int contents_phone = 0x7f0e006d;
        public static final int contents_sms = 0x7f0e006e;
        public static final int contents_text = 0x7f0e006f;
        public static final int history_clear_one_history_text = 0x7f0e01e7;
        public static final int history_clear_text = 0x7f0e01e8;
        public static final int history_email_title = 0x7f0e01e9;
        public static final int history_empty = 0x7f0e01ea;
        public static final int history_empty_detail = 0x7f0e01eb;
        public static final int history_send = 0x7f0e01ec;
        public static final int history_title = 0x7f0e01ed;
        public static final int menu_encode_mecard = 0x7f0e01ef;
        public static final int menu_encode_vcard = 0x7f0e01f0;
        public static final int menu_help = 0x7f0e01f1;
        public static final int menu_history = 0x7f0e01f2;
        public static final int menu_settings = 0x7f0e01f3;
        public static final int menu_share = 0x7f0e01f4;
        public static final int msg_bulk_mode_scanned = 0x7f0e01f6;
        public static final int msg_camera_framework_bug = 0x7f0e01f7;
        public static final int msg_default_format = 0x7f0e01f8;
        public static final int msg_default_meta = 0x7f0e01f9;
        public static final int msg_default_mms_subject = 0x7f0e01fa;
        public static final int msg_default_status = 0x7f0e01fb;
        public static final int msg_default_time = 0x7f0e01fc;
        public static final int msg_default_type = 0x7f0e01fd;
        public static final int msg_encode_contents_failed = 0x7f0e01fe;
        public static final int msg_google_books = 0x7f0e01ff;
        public static final int msg_google_product = 0x7f0e0200;
        public static final int msg_intent_failed = 0x7f0e0201;
        public static final int msg_redirect = 0x7f0e0202;
        public static final int msg_sbc_book_not_searchable = 0x7f0e0203;
        public static final int msg_sbc_failed = 0x7f0e0204;
        public static final int msg_sbc_no_page_returned = 0x7f0e0205;
        public static final int msg_sbc_page = 0x7f0e0206;
        public static final int msg_sbc_results = 0x7f0e0207;
        public static final int msg_sbc_searching_book = 0x7f0e0208;
        public static final int msg_sbc_snippet_unavailable = 0x7f0e0209;
        public static final int msg_share_explanation = 0x7f0e020a;
        public static final int msg_share_text = 0x7f0e020b;
        public static final int msg_sure = 0x7f0e020c;
        public static final int msg_unmount_usb = 0x7f0e020d;
        public static final int place_autocomplete_clear_button = 0x7f0e0214;
        public static final int place_autocomplete_search_hint = 0x7f0e0215;
        public static final int preferences_actions_title = 0x7f0e0216;
        public static final int preferences_auto_focus_title = 0x7f0e0217;
        public static final int preferences_bulk_mode_summary = 0x7f0e0218;
        public static final int preferences_bulk_mode_title = 0x7f0e0219;
        public static final int preferences_copy_to_clipboard_title = 0x7f0e021a;
        public static final int preferences_custom_product_search_summary = 0x7f0e021b;
        public static final int preferences_custom_product_search_title = 0x7f0e021c;
        public static final int preferences_decode_1D_title = 0x7f0e021d;
        public static final int preferences_decode_Data_Matrix_title = 0x7f0e021e;
        public static final int preferences_decode_QR_title = 0x7f0e021f;
        public static final int preferences_device_bug_workarounds_title = 0x7f0e0220;
        public static final int preferences_disable_continuous_focus_summary = 0x7f0e0221;
        public static final int preferences_disable_continuous_focus_title = 0x7f0e0222;
        public static final int preferences_disable_exposure_title = 0x7f0e0223;
        public static final int preferences_front_light_auto = 0x7f0e0224;
        public static final int preferences_front_light_off = 0x7f0e0225;
        public static final int preferences_front_light_on = 0x7f0e0226;
        public static final int preferences_front_light_summary = 0x7f0e0227;
        public static final int preferences_front_light_title = 0x7f0e0228;
        public static final int preferences_general_title = 0x7f0e0229;
        public static final int preferences_invert_scan_summary = 0x7f0e022a;
        public static final int preferences_invert_scan_title = 0x7f0e022b;
        public static final int preferences_name = 0x7f0e022c;
        public static final int preferences_play_beep_title = 0x7f0e022d;
        public static final int preferences_remember_duplicates_summary = 0x7f0e022e;
        public static final int preferences_remember_duplicates_title = 0x7f0e022f;
        public static final int preferences_result_title = 0x7f0e0230;
        public static final int preferences_scanning_title = 0x7f0e0231;
        public static final int preferences_search_country = 0x7f0e0232;
        public static final int preferences_supplemental_summary = 0x7f0e0233;
        public static final int preferences_supplemental_title = 0x7f0e0234;
        public static final int preferences_try_bsplus = 0x7f0e0235;
        public static final int preferences_try_bsplus_summary = 0x7f0e0236;
        public static final int preferences_vibrate_title = 0x7f0e0237;
        public static final int result_address_book = 0x7f0e0238;
        public static final int result_calendar = 0x7f0e0239;
        public static final int result_email_address = 0x7f0e023a;
        public static final int result_geo = 0x7f0e023b;
        public static final int result_isbn = 0x7f0e023c;
        public static final int result_product = 0x7f0e023d;
        public static final int result_sms = 0x7f0e023e;
        public static final int result_tel = 0x7f0e023f;
        public static final int result_text = 0x7f0e0240;
        public static final int result_uri = 0x7f0e0241;
        public static final int result_wifi = 0x7f0e0242;
        public static final int sbc_name = 0x7f0e0243;
        public static final int wifi_changing_network = 0x7f0e024c;
        public static final int wifi_ssid_label = 0x7f0e024d;
        public static final int wifi_type_label = 0x7f0e024e;
    }
    public static final class style {
        private style() {}

        public static final int CaptureTheme = 0x7f0f00a7;
        public static final int ResultButton = 0x7f0f00c2;
        public static final int ShareButton = 0x7f0f00d1;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] LoadingImageView = { 0x7f040057, 0x7f0400c1, 0x7f0400c2 };
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] MapAttrs = { 0x7f040029, 0x7f040050, 0x7f040051, 0x7f040052, 0x7f040053, 0x7f040054, 0x7f0400e7, 0x7f0400ec, 0x7f040181, 0x7f040182, 0x7f040183, 0x7f040184, 0x7f040185, 0x7f040186, 0x7f040187, 0x7f040189, 0x7f040196 };
        public static final int MapAttrs_ambientEnabled = 0;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraTargetLat = 2;
        public static final int MapAttrs_cameraTargetLng = 3;
        public static final int MapAttrs_cameraTilt = 4;
        public static final int MapAttrs_cameraZoom = 5;
        public static final int MapAttrs_liteMode = 6;
        public static final int MapAttrs_mapType = 7;
        public static final int MapAttrs_uiCompass = 8;
        public static final int MapAttrs_uiMapToolbar = 9;
        public static final int MapAttrs_uiRotateGestures = 10;
        public static final int MapAttrs_uiScrollGestures = 11;
        public static final int MapAttrs_uiTiltGestures = 12;
        public static final int MapAttrs_uiZoomControls = 13;
        public static final int MapAttrs_uiZoomGestures = 14;
        public static final int MapAttrs_useViewLifecycle = 15;
        public static final int MapAttrs_zOrderOnTop = 16;
        public static final int[] SignInButton = { 0x7f04004b, 0x7f040069, 0x7f040118 };
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
    public static final class xml {
        private xml() {}

        public static final int preferences = 0x7f110003;
    }
}
